Customer digital wallets and bank accounts
==========================================

Sign-up (Customer on-boarding)
------------------------------

Sing-up (Business on-boarding)
------------------------------

Sing-in (2FA)
-------------

Customer due dilligence (KYC)
-----------------------------

Opening bank account
--------------------

Opening digital asset account
-----------------------------

Customer profile
----------------

Transferring money to the account (Top-Up)
------------------------------------------

Transferring digital asset to the account
-----------------------------------------

Withdrawing funds from the account (WWithdrawal)
------------------------------------------------

Withdrawing digital asset to the account
----------------------------------------

Internal transfers
------------------

Digital assets exchange operations
----------------------------------

Managing customer digital wallets and bank accounts
===================================================

Manually creating a business account
------------------------------------

Bank and digital account management
------------------------------------

KYC management workflow and approval
------------------------------------

Managing customer's profiles
----------------------------

Manual money transfers
----------------------

Setting commission and fee
--------------------------

Reporting and BI system (Account balances, Entries, Turn-over, etc)
-------------------------------------------------------------------











